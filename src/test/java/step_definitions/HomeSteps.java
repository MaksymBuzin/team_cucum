package step_definitions;

import io.cucumber.java.en.And;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.junit.Assert;
import org.openqa.selenium.By;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CommonPage;
import pages.HomePage;
import utils.CucumberLogUtils;
import utils.ExcelUtils;
import utils.SeleniumUtils;
import utils.WebDriverManager;

import java.io.IOException;
import java.util.Arrays;

public class HomeSteps implements CommonPage {
    HomePage homePage;

    public HomeSteps() {
        homePage = new HomePage();
    }

    @Then("Verify {string} link is displayed")
    public void verifyLinkIsDisplayed(String linkName) {
        Assert.assertTrue(WebDriverManager.isDisplayed(By.xpath(String.format(XPATH_TEMPLATE_LINKTEXT, linkName))));
    }

    @Then("Verify {string} link is enabled")
    public void verifyLinkIsEnabled(String linkLanguage) {
        WebDriverManager.getDriver().findElement(By.xpath(String.format(XPATH_TEMPLATE_LINKTEXT, linkLanguage)));
    }

    @Then("user can click dropdown for change language")
    public void userCanClickDropdownForChangeLanguage() {
        WebDriverManager.click(homePage.dropdownLanguage);
    }

    @When("User clicks on {string}")
    public void user_clicks_on(String linkBtn) {
        WebDriverManager.click(By.xpath(String.format(XPATH_TEMPLATE_LINKTEXT, linkBtn)));
    }

    @When("User switches to the next window")
    public void user_switches_to_the_next_window() {
        SeleniumUtils.switchToNextWindow();
    }

    @Then("title should be {string}")
    public void title_should_be(String title) {
        Assert.assertTrue(title.contains("Advance System - Home"));
    }

    @Then("Verify street address is {string}")
    public void verifyStreetAddressIs(String address) {
        Assert.assertTrue(WebDriverManager.isDisplayed(By.xpath(String.format(XPATH_TEMPLATE_TEXT, address))));
    }

    @And("Verify city & state address is {string}")
    public void verifyCityStateAddressIs(String cityState) {
        Assert.assertTrue(WebDriverManager.isDisplayed(By.xpath(String.format(XPATH_TEMPLATE_TEXT, cityState))));
    }

    @And("Verify phone number is {string}")
    public void verifyPhoneNumberIs(String number) {
        Assert.assertTrue(WebDriverManager.isDisplayed(By.xpath(String.format(XPATH_TEMPLATE_TEXT, number))));
    }

    @When("User scrolls down page to the end")
    public void userScrollsDownThePageToTheEnd() {
        SeleniumUtils.moveIntoView(homePage.footerCopyrihgt);
        SeleniumUtils.highlightElement(homePage.footerCopyrihgt);
        SeleniumUtils.waitForElementVisibility(homePage.joinUsBtn);
    }

    @Then("Verify secondary header {string} link is displayed")
    public void verifySecondaryHeaderLinkIsDisplayed(String linkName) {
        Assert.assertTrue(WebDriverManager.isDisplayed(By.xpath(String.format(XPATH_TEMPLATE_STICKYHEADER_LINKS,linkName))));
    }

    @Then("User clicks link {string} on secondary navigation bar\"")
    public void userClicksOnSecondaryNavigationBar(String link) {
        WebDriverManager.click(By.xpath(String.format(XPATH_TEMPLATE_STICKYHEADER_LINKS,link)));
    }

    @Then("Verify first main header image is present")
    public void verifyUserCanSeeFirstImage() {
        Assert.assertEquals("images/main-slider/image-1.jpg", homePage.firstParallaxImage.getAttribute("src"));
    }

    @Then("Verify User can see second image after fifteen sec on main header")
    public void verifyUserCanSeeSecondImageAfterFifteenSec() {
       // WebDriverWait wait = new WebDriverWait(WebDriverManager.getDriver(), 500L);
      //  wait.until(ExpectedConditions.visibilityOf(homePage.secondParallaxImage));
        Assert.assertEquals("images/main-slider/image-2.jpg", homePage.secondParallaxImage.getAttribute("src"));
    }

    @Then("Verify {string} is present")
    public void verifyIsPresent(String header) {
        String textInOneLine;
        switch (header) {
            case "first image header":
                textInOneLine = WebDriverManager.getText(homePage.headerFirstImage).replace("\n", " ");
                Assert.assertTrue(textInOneLine.equals("A bright career is waiting for you..."));
                break;
            case "first image description":
                textInOneLine = WebDriverManager.getText(homePage.descriptionFirstImage).replace("\n", " ");
                Assert.assertTrue(textInOneLine.equals("Our Career Network will enhance your job search and application process. Whether you choose to apply or just leave your information, we look forward to staying connected with you."));
                break;
            case "second image header":
                textInOneLine = WebDriverManager.getText(homePage.headerSecondImage).replace("\n", " ");
                Assert.assertTrue(textInOneLine.equals("Think Big. Achieve Bigger."));
                break;
            case "second image description":
                textInOneLine = WebDriverManager.getText(homePage.descriptionSecondImage);
                Assert.assertTrue(textInOneLine.equals("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s."));
                break;
            default:
                System.out.println("No such data");
        }
    }

    @Then("Verify title of the page is {string}")
    public void verifyTitleOfThePageIs(String title) {
        Assert.assertEquals(title, WebDriverManager.getDriver().getTitle());
    }

    @Then("Verify header {string} is displayed")
    public void verifyHeaderIsDisplayed(String Header) {
        Assert.assertTrue(WebDriverManager.isDisplayed(By.xpath(String.format(XPATH_TEMPLATE_TEXT, Header))));
    }

    @And("Verify description text {string} is displayed")
    public void verifyDescriptionTextIsDisplayed(String Description) {
        Assert.assertTrue(WebDriverManager.isDisplayed(By.xpath(String.format(XPATH_TEMPLATE_TEXT, Description))));
    }

    @Then("Verify h2 {string} header is displayed")
    public void verifyH2HeaderIsDisplayed(String h2Header) {
        Assert.assertTrue(WebDriverManager.isDisplayed(By.xpath(String.format(XPATH_TEMPLATE_LINKTEXT_H2, h2Header))));
    }

    @When("User scrolls to see next testimonial")
    public void user_scrolls_to_see_next_testimonial() {
        WebDriverManager.click(homePage.rightArrowBtnTestimon);
    }

    @Then("Verify there is a testimonial that belongs to {string} from {string}")
    public void verify_there_is_a_testimonial_that_belongs_to_from(String name, String city) {
        switch (name) {
            case "John Smith":
                Assert.assertEquals(homePage.johnSmithTestimon, WebDriverManager.getText(homePage.activeTestimon));
                Assert.assertEquals(name, WebDriverManager.getText(homePage.nameTestimonAuthor));
                Assert.assertEquals(city, WebDriverManager.getText(homePage.cityTestimonAuthor));
                break;
            case "Jenifer Hearly":
                Assert.assertEquals(homePage.jeniferHearlyTestimon, WebDriverManager.getText(homePage.activeTestimon));
                Assert.assertEquals(name, WebDriverManager.getText(homePage.nameTestimonAuthor));
                Assert.assertEquals(city, WebDriverManager.getText(homePage.cityTestimonAuthor));
                break;
        }
    }

    @Then("Verify scroll up btn is present")
    public void verifyScrollUpBtnIsPresent() {
        Assert.assertTrue(WebDriverManager.isDisplayed(homePage.scrollUpBtn));
    }

    @And("User clicks scroll up btn")
    public void userClicksScrollUpBtn() {
        WebDriverManager.click(homePage.scrollUpBtn);
    }

    @Then("Verify User sees top content")
    public void verifyUserSeesTopContent() {
        Assert.assertTrue(homePage.topbarMenue.isDisplayed());
    }

    @Then("User clicks {string} in footer section\"")
    public void userClicksInFooterSection(String link) {
        WebDriverManager.click(By.xpath(String.format(XPATH_TEMPLATE_FOOTER_LINKS,link)));
    }

    @Then("Verify footer {string} is displayed")
    public void verifyFooterIsDisplayed(String link) {
        WebDriverManager.isDisplayed(By.xpath(String.format(XPATH_TEMPLATE_FOOTER_LINKS,link)));
    }

    @Then("Verify {string} is displayed under header")
    public void verifyIsDisplayedUnderHeader(String des) {
        switch (des) {
            case "Leadership Development":
                Assert.assertTrue(WebDriverManager.isDisplayed(homePage.textDescription_1));
                break;
            case "Capability Building":
                Assert.assertTrue(WebDriverManager.isDisplayed(homePage.textDescription_2));
                break;
            case "Reward & Benefits":
                Assert.assertTrue(WebDriverManager.isDisplayed(homePage.textDescription_3));
                break;
            case "Employee & Industrial":
                Assert.assertTrue(WebDriverManager.isDisplayed(homePage.textDescription_4));
                break;
            case "Delivering Excellent":
                Assert.assertTrue(WebDriverManager.isDisplayed(homePage.textDescription_5));
                break;
        }
    }

    @Then("Verify social media link {string} is displayed in footer")
    public void verifySocialMediaLinkIsDisplayedInFooter (String link){
        Assert.assertTrue(WebDriverManager.isDisplayed(By.xpath(String.format(XPATH_TEMPLATE_SOCIALMEDIA_FOOTER, link))));
    }

    @Then("Verify email input field is present")
    public void verifyEmailInputFieldIsPresent() {
        Assert.assertEquals("Email Address...",WebDriverManager.getAtribute(homePage.emailInputField,"placeholder"));
    }

    @Then("User clicks social media link {string} in footer section")
    public void userClicksSocialMediaLinkInFooterSection (String link){
        WebDriverManager.click(By.xpath(String.format(XPATH_TEMPLATE_SOCIALMEDIA_FOOTER, link)));
    }

    @Then("Verify description as {string} is displayed under header")
    public void verifyDescriptionAsIsDisplayedUnderHeader (String des){
        switch (des) {
            case "Leadership Development":
                Assert.assertTrue(WebDriverManager.isDisplayed(homePage.textDescription_1));
                break;
                case "Capability Building":
                    Assert.assertTrue(WebDriverManager.isDisplayed(homePage.textDescription_2));
                    break;
                case "Reward & Benefits":
                    Assert.assertTrue(WebDriverManager.isDisplayed(homePage.textDescription_3));
                    break;
                case "Employee & Industrial":
                    Assert.assertTrue(WebDriverManager.isDisplayed(homePage.textDescription_4));
                    break;
                case "Delivering Excellent":
                    Assert.assertTrue(WebDriverManager.isDisplayed(homePage.textDescription_5));
                    break;
            }
        }


    @Then("Verify footer information as {string} is displayed")
        public void verifyFooterInformationAsIsDisplayed (String info){
            switch (info) {
                case "Address: 10090 Main St, Fairfax, VA":
                    Assert.assertTrue(WebDriverManager.isEnabled(homePage.infoAddress));
                    break;
                case "Phone: +1 703-831-3217":
                    Assert.assertTrue(WebDriverManager.isDisplayed(homePage.infoPhone));
                    break;
                case "Email: Info@advancesystems.us":
                    Assert.assertTrue(WebDriverManager.isDisplayed(homePage.infoEmail));
                    break;
                case "Mon to Sat: 9.00 am to 5:00 pm":
                    Assert.assertTrue(WebDriverManager.isDisplayed(homePage.infoHour));
                    break;
            }
        }

}
