package step_definitions;

import com.google.gson.internal.bind.util.ISO8601Utils;
import io.cucumber.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.AboutUsPage;
import pages.CommonPage;
import pages.HomePage;
import utils.SeleniumUtils;
import utils.WebDriverManager;

import java.util.List;
import java.util.Locale;
import java.util.Map;

public class AboutUsPageSteps implements CommonPage {
    HomePage homepage;
    AboutUsPage aboutUsPage;

    public AboutUsPageSteps() {
        homepage = new HomePage();
        aboutUsPage = new AboutUsPage();
    }

    @Then("Verify name {string} under main header")
    public void verify_name_under_main_header(String name) {
        Assert.assertEquals(name, WebDriverManager.getText(aboutUsPage.authorNameUnerMainHeader));
    }

    @Then("Verify title of the person under main header is {string}")
    public void verify_title_of_the_person_under_main_header_is(String title) {
        Assert.assertEquals(title, WebDriverManager.getText(aboutUsPage.authorTitleUnerMainHeader));
    }

    @And("Verify Meet Our Experts block has {int} experts")
    public void verifyMeetOurExpertsBlockHasExperts(int experts) {
        Assert.assertEquals(experts, aboutUsPage.meetOurExpertsBlock.size());
    }


    @Then("Verify {string} expert has following credentials displayed:  {string}, {string}, {string}, {string}, {string}")
    public void verifyExpertHasFollowingCredentialsDisplayed(String expertNum, String image, String name, String email, String title, String quote) {
        List<WebElement> allExp = aboutUsPage.meetOurExpertsBlock;

        String name1 = allExp.get(Integer.parseInt(expertNum) - 1).findElement(By.xpath(".//h3")).getText();
        String title1 = allExp.get(Integer.parseInt(expertNum) - 1).findElement(By.className("designation")).getText();
        String quote1 = allExp.get(Integer.parseInt(expertNum) - 1).findElement(By.className("text")).getText().replace("\n", " ");
        String email1 = allExp.get(Integer.parseInt(expertNum) - 1).findElement(By.xpath(".//div[@class='mail']")).getText();
        String image1 = allExp.get(Integer.parseInt(expertNum) - 1).findElement(By.xpath(".//div[@class='image']/img")).getAttribute("src");

        Assert.assertTrue(image1.contains(image));
        Assert.assertEquals(name1, name);
        Assert.assertEquals(email1, email);
        Assert.assertEquals(title, title1);
        Assert.assertEquals(quote1, quote);

    }

    @Then("Verify {string} expert has following links: {string}, {string}, {string},{string}")
    public void verifyExpertHasFollowingLinks(String expertNum, String arg1, String arg2, String arg3, String arg4) {
        List<WebElement> allExp = WebDriverManager.getDriver().findElements(By.xpath("//div[@class='team-member-one col-lg-3 col-md-6 col-xs-12']"));

        List<WebElement> allLinks = allExp.get(0).findElements(By.xpath(".//li/a"));
        allLinks.forEach(x -> Assert.assertTrue(WebDriverManager.isEnabled(x)));


    }

    @Then("Verify first expert has social media link {string} displayed")
    public void verifyFirstExpertHasSocialMediaLinkDisplayed(String link) {
        Assert.assertTrue(WebDriverManager.isDisplayed(By.xpath(String.format(XPATH_TEMPLATE_FIRST_EXPERT_LINKS, link))));
    }

    @And("User clicks on link {string} in first expert block")
    public void userClicksOnLinkInFirstExpertBlock(String link) {
     WebDriverManager.click(By.xpath(String.format(XPATH_TEMPLATE_FIRST_EXPERT_LINKS, link)));
    }
}
























