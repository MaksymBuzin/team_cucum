package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.WebDriverManager;

public class HomePage {
    public HomePage() {
        PageFactory.initElements(WebDriverManager.getDriver(), this);
    }

    @FindBy(xpath = "//a[text()='Join Us']")
    public WebElement joinUsBtn;

    @FindBy(xpath = "//div[@class='sticky-header']//a[text()='Home']")
    public WebElement homeStickyHeaderLink;

    @FindBy(xpath = "//div[@class='sticky-header']//a[text()='About Us']")
    public WebElement aboutUsStickyHeaderLink;

    @FindBy(xpath = "//div[@class='sticky-header']//a[text()='Services']")
    public WebElement servicesStickyHeaderLink;

    @FindBy(xpath = "//div[@class='sticky-header']//a[text()='Clients']")
    public WebElement clientsStickyHeaderLink;

    @FindBy(xpath = "//div[@class='sticky-header']//a[text()='Solutions']")
    public WebElement solutionsStickyHeaderLink;

    @FindBy(xpath = "//div[@class='sticky-header']//a[text()='Join Us']")
    public WebElement joinUsStickyHeaderLink;

    @FindBy(xpath = "//div[@class='sticky-header']//a[text()='Contact Us']")
    public WebElement contactUsStickyHeaderLink;

    @FindBy(xpath = "(//h2[@class='white-heading'])[1]")
    public WebElement headerFirstImage;

    @FindBy(xpath = "(//h2[@class='white-heading'])[2]")
    public WebElement headerSecondImage;

    @FindBy(xpath = "//div[@class='text' and contains(text(),'Our Career ')] ")
    public WebElement descriptionFirstImage;

    @FindBy(xpath = "//div[@class='tp-loop-wrap']//div[@class='text' and contains(text(),'Lorem ')]")
    public WebElement descriptionSecondImage;

    @FindBy(xpath = "//div[contains(@src,'image-1')]")
    public WebElement firstParallaxImage;

    @FindBy(xpath = "//div[contains(@src,'image-2')]")
    public WebElement secondParallaxImage;

    //=====================Testimonial Section =======================================
    @FindBy(xpath = "//div[@class='owl-item active']//div[@class='text']")
    public WebElement activeTestimon;

    @FindBy(xpath = "//div[@class='owl-item active']//h3")
    public WebElement nameTestimonAuthor;

    @FindBy(xpath = "//div[@class='owl-item active']//div[@class='designation']")
    public WebElement cityTestimonAuthor;

    @FindBy(xpath = "//span[@class='icon-arrows']")
    public WebElement rightArrowBtnTestimon;

    public String johnSmithTestimon = "“ These guys are just the coolest company ever! Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ”.";
    public String jeniferHearlyTestimon = "“ These guys are just the coolest company ever! Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer scrambled it to make a type specimen book. ”.";
    // ===============================================================================
    @FindBy(xpath = "//span[@class='icon fa fa-arrow-up']")
    public WebElement scrollUpBtn;

    @FindBy(className="topbar-menu")
    public WebElement topbarMenue;

    //======================Footer related elements====================================
    @FindBy(className = "footer-copyright")
    public WebElement footerCopyrihgt;

    @FindBy(xpath = "//div[contains(@class,'footer-link footer-column-border')]//a[text()='Home']")
    public WebElement homeFooterLink;

    @FindBy(xpath = "//div[contains(@class,'footer-link footer-column-border')]//a[text()='About Us']")
    public WebElement aboutUsFooterLink;

    @FindBy(xpath = "//div[contains(@class,'footer-link footer-column-border')]//a[text()='Services']")
    public WebElement servicesFooterLink;

    @FindBy(xpath = "//div[contains(@class,'footer-link footer-column-border')]//a[text()='Clients']")
    public WebElement clientsFooterLink;

    @FindBy(xpath = "//div[contains(@class,'footer-link footer-column-border')]//a[text()='Solutions']")
    public WebElement solutionsFooterLink;

    @FindBy(xpath = "//div[contains(@class,'footer-link footer-column-border')]//a[text()='Join Us']")
    public WebElement joinUsFooterLink;

    @FindBy(xpath = "//div[contains(@class,'footer-link footer-column-border')]//a[text()='Contact Us']")
    public WebElement contactUsFooterLink;

    @FindBy(xpath = "//input[@type='email']")
    public WebElement emailInputField;
    //==========================Social media links=========================================
    @FindBy(xpath = "//ul[@class='social-icon-seven']//a[@href='https://facebook.com']")
    public WebElement facebookNavBarLink;

    @FindBy(xpath = "//ul[@class='social-icon-seven']//a[@href='https://twitter.com']")
    public WebElement twitterNavBarLink;

    @FindBy(xpath = "//ul[@class='social-icon-seven']//a[@href='https://skype.com']")
    public WebElement skypeNavBarLink;

    @FindBy(xpath = "//ul[@class='social-icon-seven']//a[@href='https://linkedin.com']")
    public WebElement linkedinNavBarLink;

    @FindBy(xpath = "//ul[@class='social-icon-six']//a[@href='https://facebook.com']")
    public WebElement facebookFooterLink;

    @FindBy(xpath = "//ul[@class='social-icon-six']//a[@href='https://twitter.com']")
    public WebElement twitterFooterLink;

    @FindBy(xpath = "//ul[@class='social-icon-six']//a[@href='https://skype.com']")
    public WebElement skypeFooterLink;

    @FindBy(xpath = "//ul[@class='social-icon-six']//a[@href='https://linkedin.com']")
    public WebElement linkedinFooterLink;

    //===========================Description under h3_Header===============================
    @FindBy(xpath = "//div[@class='flex-box']/div[1]//div[@class='text']")
    public WebElement textDescription_1;

    @FindBy(xpath = "//div[@class='flex-box']/div[2]//div[@class='text']")
    public WebElement textDescription_2;

    @FindBy(xpath = "//div[@class='flex-box']/div[3]//div[@class='text']")
    public WebElement textDescription_3;

    @FindBy(xpath = "//div[@class='flex-box']/div[4]//div[@class='text']")
    public WebElement textDescription_4;

    @FindBy(xpath = "//div[@class='flex-box']/div[5]//div[@class='text']")
    public WebElement textDescription_5;
    //=============================================================================

    @FindBy(xpath = "//*[@class='icon fa fa-angle-down']")
    public WebElement dropdownLanguage;

    @FindBy(xpath = "//div[@class='footer-about footer-column-border col-lg-4 col-md-6 col-sm-6 col-xs-12']")
    public WebElement footerInfo;

    //================================Information===================================
    @FindBy(xpath = "//*[@class='list-info']//li[1]")
    public WebElement infoAddress;

    @FindBy(xpath = "//*[@class='list-info']//li[2]")
    public WebElement infoPhone;

    @FindBy(xpath = "//*[@class='list-info']//li[3]")
    public WebElement infoEmail;

    @FindBy(xpath = "//*[@class='list-info']//li[4]")
    public WebElement infoHour;
}
