package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.WebDriverManager;

import java.util.List;

public class AboutUsPage {
    public AboutUsPage() {
        PageFactory.initElements(WebDriverManager.getDriver(), this);
    }

    @FindBy(xpath="//div[@class='author-information']/h4")
    public WebElement authorNameUnerMainHeader;

    @FindBy(xpath="//div[@class='author-information']/div")
    public WebElement authorTitleUnerMainHeader;

    @FindBy(className="team-member-one")
    public List<WebElement> meetOurExpertsBlock;


}
