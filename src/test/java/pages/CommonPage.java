package pages;

public interface CommonPage {

    String XPATH_TEMPLATE_BUTTON = "//button[text()='%s']";
    String XPATH_TEMPLATE_LINKTEXT = "//a[text()='%s']";
    String XPATH_TEMPLATE_TEXT = "//*[text()=\"%s\"]";
    String XPATH_TEMPLATE_TEXT_CONTAINS = "//*[contains(text(), '%s')]";
    String XPATH_TEMPLATE_INPUT_FIELD = "//input[@placeholder='%s']";
    String XPATH_TEMPLATE_LINKTEXT_H1 = "//h1[text()='%s']";
    String XPATH_TEMPLATE_LINKTEXT_H2 ="//h2[text()='%s']";
    String XPATH_TEMPLATE_SOCIALMEDIA_FOOTER = "//ul[@class='social-icon-six']//a[@href='https://%s.com']";
    String XPATH_TEMPLATE_FOOTER_LINKS="//div[contains(@class,'footer-link footer-column-border')]//a[text()='%s']";
    String XPATH_TEMPLATE_STICKYHEADER_LINKS ="//div[@class='sticky-header']//a[text()='%s']";
    String XPATH_TEMPLATE_FIRST_EXPERT_LINKS ="//div[@class='team-member-one col-lg-3 col-md-6 col-xs-12'][1]//a[contains(@href,'https://%s.com')]";

}
