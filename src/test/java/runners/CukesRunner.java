package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber-report.html",
                "json:target/cucumber-report/Cucumber.json",
                "junit:target/cucumber-report/Cucumber.xml",
        },

        publish = true,
        features = "src/test/resources/features",
        glue = {"step_definitions"},
        stepNotifications = true,
        dryRun = false,
        monochrome = true,
        tags = "@ADVSYS-5"

)

public class CukesRunner {
}
