@HomePageScen
Feature:Home page related scenarios

  @ADVSYS-5 @Smoke
  Scenario: Verify title of the Home page
    Then Verify title of the page is "Advance Systems - Home"

  @ADVSYS-6 @Smoke
  Scenario Outline: Verify following link is displayed
    Then Verify "<linkText>" link is displayed
    Examples:
      | linkText    |
      | Get Support |
      | Job Career  |
      | Feedback    |

  @ADVSYS-6
  Scenario Outline: Verify Language section user click dropdown and able to see English, Spanish, French
    When user can click dropdown for change language
    Then Verify "<linkText>" link is enabled
    Examples:
      | linkText |
      | English  |
      | Spanish  |
      | French   |

  @ADVSYS-7 @Smoke
  Scenario: Verify home page has current street address and phone number
    Then Verify street address is "10090 Main Street"
    And Verify city & state address is "Fairfax, VA, USA"
    And Verify phone number is "+1 703-831-3216"

  @ADVSYS-9
  Scenario Outline:Verify Page Navigation Bar has following links displayed
    Then Verify "<linkText>" link is displayed
    Examples:
      | linkText   |
      | Home       |
      | About Us   |
      | Services   |
      | Clients    |
      | Solutions  |
      | Join Us    |
      | Contact Us |

  @ADVSYS-9
  Scenario Outline:Verify Page Navigation Bar links take to corresponding pages by verifying title
    When User clicks on "<link>"
    Then Verify title of the page is "<title>"
    Examples:
      | link       | title                        |
      | Home       | Advance Systems - Home       |
      | About Us   | Advance Systems - About Us   |
      | Services   | Advance Systems - Services   |
      | Clients    | Advance Systems - Clients    |
      | Solutions  | Advance Systems - Solutions  |
      | Join Us    | Advance Systems - Join Us    |
      | Contact Us | Advance Systems - Contact Us |

  @ADVSYS-9
  Scenario Outline:Verify secondary Navigation bar is visible after user scrolls down the page
    When User scrolls down page to the end
    Then Verify secondary header "<linkText>" link is displayed
    Examples:
      | linkText   |
      | Home       |
      | About Us   |
      | Services   |
      | Clients    |
      | Solutions  |
      | Join Us    |
      | Contact Us |

  @ADVSYS-9
  Scenario Outline:Verify secondary Navigation bar links take to corresponding pages by verifying title(after scrolling down)
    When User scrolls down page to the end
    Then User clicks link "<link>" on secondary navigation bar"
    Then Verify title of the page is "<title>"
    Examples:
      | link       | title                        |
      | Home       | Advance Systems - Home       |
      | About Us   | Advance Systems - About Us   |
      | Services   | Advance Systems - Services   |
      | Clients    | Advance Systems - Clients    |
      | Solutions  | Advance Systems - Solutions  |
      | Join Us    | Advance Systems - Join Us    |
      | Contact Us | Advance Systems - Contact Us |

  @ADVSYS-11
  Scenario: Verify Read More btn is displayed
    Then Verify "Read More" link is displayed

  @ADVSYS-11
  Scenario: Verify Read More btn takes the user to "Services" page.
    When User clicks on "Read More"
    Then Verify title of the page is "Advance Systems - Services"

  @ADVSYS-11
  Scenario: Verify there is a section under nav bar with header
    Then Verify "first image header" is present

  @ADVSYS-11
  Scenario: Verify there is a section under nav bar with description
    Then Verify "first image description" is present

  @ADVSYS-11
  Scenario: Verify image content under nav bar refreshes every 10-15 sec
    Then Verify first main header image is present
    Then Verify User can see second image after fifteen sec on main header

  @ADVSYS-11
  Scenario: Verify image content under nav bar refreshes every 10-15 sec with new header and description
    Then Verify first main header image is present
    And Verify "first image header" is present
    And Verify "first image description" is present
    Then Verify User can see second image after fifteen sec on main header
    And Verify "second image header" is present
    And Verify "second image description" is present

  @ADVSYS-13
  Scenario:There should be a section "Words from our Clients"
    Then Verify h2 "Words from our Clients" header is displayed

  @ADVSYS-13
  Scenario:Verify there is a testimonial, author's name and city and they change
    Then Verify there is a testimonial that belongs to "John Smith" from "New York"
    When User scrolls to see next testimonial
    Then Verify there is a testimonial that belongs to "Jenifer Hearly" from "New York"

  @ADVSYS-15
  Scenario Outline: Following information should be displayed in footer section:
    When User scrolls down page to the end
    Then Verify footer information as "<information>" is displayed
    Examples:
      | information                         |
      | Address: 10090 Main St, Fairfax, VA |
      | Phone: +1 703-831-3217              |
      | Email: Info@advancesystems.us       |
      | Mon to Sat: 9.00 am to 5:00 pm      |

  @ADVSYS-16
  Scenario Outline:Verify footer's links are displayed
    When User scrolls down page to the end
    Then Verify footer "<link>" is displayed
    Examples:
      | link       |
      | Home       |
      | About Us   |
      | Services   |
      | Clients    |
      | Contact Us |
      | Join Us    |

  @ADVSYS-16
  Scenario Outline:Verify footer's links take to corresponding pages
    When User scrolls down page to the end
    Then User clicks "<link>" in footer section"
    Then Verify title of the page is "<title>"
    Examples:
      | link       | title                        |
      | Home       | Advance Systems - Home       |
      | About Us   | Advance Systems - About Us   |
      | Services   | Advance Systems - Services   |
      | Clients    | Advance Systems - Clients    |
      | Contact Us | Advance Systems - Contact Us |
      | Join Us    | Advance Systems - Join Us    |

  @ADVSYS-17
  Scenario Outline: Verify social links are displayed in footer section
    When User scrolls down page to the end
    Then Verify social media link "<link>" is displayed in footer
    Examples:
      | link     |
      | facebook |
      | twitter  |
      | skype    |
      | linkedin |

  @ADVSYS-17
  Scenario Outline: Verify social links in footer section take to correct pages
    When User scrolls down page to the end
    And User clicks social media link "<link>" in footer section
    When User switches to the next window
    Then Verify title of the page is "<title>"
    Examples:
      | link     | title                                                   |
      | facebook | Facebook - log in or sign up                            |
      | twitter  |                                                         |
      | skype    | Skype \| Stay connected with free video calls worldwide |
      | linkedin | LinkedIn: Log In or Sign Up                             |

  @ADVSYS-18
  Scenario:Verify email input field is present
    When User scrolls down page to the end
    Then Verify email input field is present

  @ADVSYS-19
  Scenario Outline:Verify there is a button in the bottom right corner to scroll the window to top content on all pages
    When User clicks on "<link>"
    When User scrolls down page to the end
    Then Verify scroll up btn is present
    And User clicks scroll up btn
    Then Verify User sees top content
    Examples:
      | link |
      | Home |
#      | About Us   |// Temporarily inactive .Will be implemented nex sprint
#      | Services   |
#      | Clients    |
#      | Solutions  |
#      | Join Us    |
#      | Contact Us |

  @ADVSYS-22
  Scenario: Verify homepage has header, secondary header, and description text
    Then Verify header "Welcome to Advance Systems LLC." is displayed
    And Verify header "Our Mission is simple, deliver very honest recruitment services to every customer." is displayed
    And Verify description text "Day in and day out for the last years we’ve been more than just a staffing company. Throughout this time we’ve built relationships, we’ve grown together internally and externally, and have created a system that allows us to personally cater to the needs of our clients and candidates. We’ve been a mentor for some, a team builder for others, but most importantly we’ve been there. We know there is no substitute for experience, so let us help you navigate through the ever changing web of talent. " is displayed

  @ADVSYS-12
  Scenario Outline: Verify 5 section displayed as a headers and descriptions under it.
    When Verify header "<headerH3>" is displayed
    Then Verify description as "<description>" is displayed under header
    Examples:
      | headerH3               | description                                                                |
      | Leadership Development | Lorem Ipsum is simply dummy text of the printing and typesetting industry. |
      | Capability Building    | Lorem Ipsum is simply dummy text of the printing and typesetting industry. |
      | Reward & Benefits      | Lorem Ipsum is simply dummy text of the printing and typesetting industry. |
      | Employee & Industrial  | Lorem Ipsum is simply dummy text of the printing and typesetting industry. |
      | Delivering Excellent   | Lorem Ipsum is simply dummy text of the printing and typesetting industry. |





