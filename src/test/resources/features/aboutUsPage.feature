Feature: About Us page related scenarios

  @ADVSYS-33
  Scenario:Verify there is a header “Welcome to Advance Systems LLC.” on "About Us" page
    Given User clicks on "About Us"
    Then Verify h2 "Welcome to Advance Systems LLC." header is displayed

  @ADVSYS-33
  Scenario: Verify there is a name and title of the person under “Welcome to Advance Systems LLC.” header
    Given User clicks on "About Us"
    Then Verify name "Kuba Z" under main header
    Then Verify title of the person under main header is "CEO & Founder"

  @ADVSYS-34
  Scenario:There should be a section with header “Why Choose Us”
    Given User clicks on "About Us"
    Then Verify h2 "Why Choose Us" header is displayed


  @ADVSYS-34
  Scenario Outline:Verify headers displayed and and text underneath: Time Services, Experienced Team,Good Track Record
    Given User clicks on "About Us"
    Then Verify header "<headerH3>" is displayed
    And Verify description text "<description>" is displayed
    Examples:
      | headerH3          | description                                                                                                                |
      | On Time Services  | Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.              |
      | Experienced Team  | Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.   |
      | Good Track Record | Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's dummy text. |

  @ADVSYS-36
  Scenario: Verify header Under Our Experts section “We are Recruitment Experts”.
    Given User clicks on "About Us"
    Then Verify header "We are Recruitment Experts" is displayed

  @ADVSYS-36
  Scenario:Verify there is a button “Our Services” which take to Services page.
    Given User clicks on "About Us"
    And Verify "Our Services" link is displayed
    Then User clicks on "Our Services"
    Then Verify title of the page is "Advance Systems - Services"

  @ADVSYS-35
  Scenario: Verify there is a header Meet Our Experts
    When User clicks on "About Us"
    Then Verify h2 "Meet Our Experts" header is displayed

  @ADVSYS-35
  Scenario Outline:Verify Meet Our Experts Block has correct information
    When User clicks on "About Us"
    Then Verify "<expertNumber>" expert has following credentials displayed:  "<image>", "<name>", "<email>", "<title>", "<quote>"
    Examples:
      | expertNumber | image                      | name            | email                  | title      | quote                                                         |
      | 1            | images/resource/team-1.jpg | Richard Antony  | info@advancesystems.us | Founder    | Great explorer of the truth, the master human happiness.      |
      | 2            | images/resource/team-2.jpg | Charz Cuthbert  | info@advancesystems.us | President  | Teachings of the great explors of truth the master builders.. |
      | 3            | images/resource/team-3.jpg | Eliot Hananiah  | info@advancesystems.us | HR Manager | Circumstances occur in which toil pain can greatest pleasure. |
      | 4            | images/resource/team-4.jpg | Daren Eldbridge | info@advancesystems.us | Executive  | The greatest pleasur to take seds example which of us ever.   |

  @ADVSYS-35
  Scenario Outline:Verify there are social links in first expert block
    Given User clicks on "About Us"
    Then Verify first expert has social media link "<link>" displayed
    Examples:
      | link     |
      | facebook |
      | twitter  |
      | skype    |
      | linkedin |

  @ADVSYS-35
  Scenario Outline:Verify social links in first expert block take to correct pages
    Given User clicks on "About Us"
    And User clicks on link "<link>" in first expert block
    Then Verify title of the page is "<title>"
    Examples:
      | link     | title                                                   |
      | facebook | Facebook - log in or sign up                            |
      | twitter  | Twitter                                                 |
      | skype    | Skype \| Stay connected with free video calls worldwide |
      | linkedin | LinkedIn: Log In or Sign Up                             |





#  @ADVSYS-35
#  Scenario Outline:Verify there are social links under each expert's quote
#    When User clicks on "About Us"
#    Then Verify "<expertNumber>" expert has following links: "<facebook>", "<twitter>", "<skype>","<linkedin>"
#    Examples:
#      | expertNumber | facebook             | twitter             | skype             | linkedin             |
#      | 1            | https://facebook.com | https://twitter.com | https://skype.com | https://linkedin.com |
##      | 2            | https://facebook.com | https://twitter.com | https://skype.com | https://linkedin.com |
##      | 3            | https://facebook.com | https://twitter.com | https://skype.com | https://linkedin.com |
##      | 4            | https://facebook.com | https://twitter.com | https://skype.com | https://linkedin.com |






















